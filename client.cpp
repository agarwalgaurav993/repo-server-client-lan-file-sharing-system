/*Gaurav Agarwal - 20162088*/
#include <iostream>
#include <cstdio>
#include <unistd.h>
#include <algorithm>
#include <vector>
#include <map>
#include <string.h>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <time.h>

#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <signal.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <unistd.h>
#include <fcntl.h>
#define PORT_NUM 70021
using namespace std;
int extractname(string path)
{
	//int n1=strlen(path);
	int n=path.length();
	while( n>0 && path[n-1]!='/' )
		n--;
	return n;

}
/*Read Search Result*/
void getSearchResult(int sockfd,vector <string> &searchRes)
{
	
	string temp;
	char buff[256]="1";
	vector <string> ar;
	int n;
	n = write(sockfd, buff, strlen(buff));
	if(n<0){
		cout<<"Write Error\n";
	}
	char searchName[256];
	cout<<"Enter file name :\n>> ";
	cin>>searchName;
	n = write(sockfd, searchName, strlen(searchName));
	if(n<0){
		cout<<"Write Error\n";
	}
	bzero(buff,256);
	if(n=read(sockfd,buff,255))
		{}

	n=atoi(buff);
	write(sockfd,"OK",2);
	bzero(buff,256);
	int c;
	while(n--)
	{
		
		if(c=read(sockfd,buff,255)){
			temp=string(buff);
			searchRes.push_back(temp);
			bzero(buff,256);
		}
		
	}
		
	return;

}
void split(string &s, char delim, pair<string, string> &elems) {
    istringstream ss;
    ss.str(s);
    string item;
    getline(ss, item, delim) ;
    elems.first=item;
    getline(ss, item, delim) ;
    elems.second=item;
}
void send(int clientfd,string clientIP)
{
	char buff[256];
	int n;
	bzero(buff,256);
	n = read(clientfd,buff,255);
	string name(buff);
	int fd=open(buff,O_RDONLY);
	if(fd<0){
		cout<<"File do not exists anymore\n";
		write(clientfd,"N",1);
		return;
	}

	write(clientfd,"Y",1);
	bzero(buff,256);
	while(n = read(fd,buff,1)){
			
		if(n<0)
			cout<<"Read Error from File";

		
		n = write(clientfd,buff,1);
	    if (n < 0) 
	    	cout<<"ERROR writing to socket";
	    bzero(buff,256);
	}
	close(fd);
		return;
}
int socketCreateBind(int portno)
{
	/*Socket Creation*/
	int sockfd;
	struct sockaddr_in my_addr;
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if( sockfd<0 )
		cout<<"Error";
	bzero((char*) &my_addr, sizeof( my_addr ));
	my_addr.sin_family = AF_INET;
	my_addr.sin_addr.s_addr = INADDR_ANY;
	my_addr.sin_port = htons(portno);
	/*Socket Binds to address*/
	if( bind(sockfd,(struct sockaddr*) &my_addr, sizeof(my_addr)) < 0){
		cout<<"Download Server bind error\n";
		close(sockfd);
		return -1;
	}
	else
	{
		cout<< "Bind Completed\n";
	}
	return sockfd;
}
void download(int sockfd,char* filename)
{
	int t=1,n;
	char buff[256];
	bzero(buff,256);
	int flag=0;
	int pos=extractname(string(filename));
	n = write(sockfd, filename, strlen(filename));
	if(n<0){
		cout<<"Write Error\n";
	}
	n=0;
	filename=filename+pos;
	
	if(n=read(sockfd,buff,1))
	{
		if(buff[0]=='N'){
			cout<<"File do not exists anymore at this Server\n";
			return;
		}
	}
	
	bzero(buff,256);
	int fd=creat(filename, S_IRWXU);
	while(n = read(sockfd,buff,1))
	{
		write(fd,buff,n);
		bzero(buff,1);
	}
	close(fd);
	cout<<"Downloaded\n";
	return;

}
int connectToServer(int mysockfd,char* serverName,int portno)
{
	struct sockaddr_in serv_addr;
	struct hostent *server;

	/*Finding Server's IP by name*/
	server=gethostbyname(serverName);
	if (server == NULL) {
        cout<<"ERROR, no such host\n";
        close(mysockfd);
        return -1;
    }
    //Book Keeping
    bzero((char *) &serv_addr, sizeof(serv_addr));

    /*struct sockaddr_in serv_addr Initialization*/
    serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr,(char *)&serv_addr.sin_addr.s_addr,server->h_length);
	serv_addr.sin_port=htons(portno);

	/*Getting Server's IP*/
	string serverIP(inet_ntoa(serv_addr.sin_addr));
	cout<<serverIP;

	if(connect(mysockfd,(struct sockaddr*) &serv_addr,sizeof(serv_addr))==-1){
		cout<<"Server unreachable at the moment\n";
		close(mysockfd);
		return -1;
	}
	return 1;
}
void server1(int sockfd)
{
	struct sockaddr_in cli_addr;
	socklen_t clilen;
	int clientfd;
	int pid;
	while(1){
		cout<<"Download Server Listening\n";
		int pid=-1;

		listen(sockfd,5);
		clilen = sizeof(cli_addr);
	
		clientfd = accept(sockfd,(struct sockaddr*) &cli_addr, &clilen);
		if(clientfd<0){
			cout<<"Accept Error";
		}
		else{
			cout<<"Client Accepted : ";
			//pid=fork();
		}
		/*if(pid==0)
		{*/
			string clientIP(inet_ntoa(cli_addr.sin_addr));
			cout<<clientIP<<"\n";

			/*For Chatting*/
			send(clientfd,clientIP);
			cout<<"Download request served.\n";
			close(clientfd);
			/*_exit(0);
		}*/

		
	}
	_exit(0);
}
void search(int sockfd)
{
	
	vector<string> res;
	res.clear();
	getSearchResult(sockfd,res);
	if(res.size()==0)
		cout<<"No Mirrors\n";
	else{
			char n1[4];
			cout<<"Select a Mirror :\n\n";
			for(int i=0;i<res.size();i++)
			{
				cout<<i+1<<". "<<res[i].substr(extractname(res[i]))<<endl;
				
			}
			cout<<"\n>>";
			cin>>n1;
			int n=atoi(n1);
			if(n>res.size())
			{
				cout<<"Invalid Mirror Selection\n";
				return;
			}

			pair<string,string> dDetails;
			split(res[n-1],'\t',dDetails);
			/*Connecting to Download Server*/
			int downloadfd=socket(AF_INET,SOCK_STREAM,0);
			char* temp= (char*) dDetails.second.c_str();
			if(connectToServer(downloadfd,temp,PORT_NUM)<0)
			{
				cout<<"Cannot connect to Download Server\n";
				return;
			}
			temp = (char*) dDetails.first.c_str();
			download(downloadfd,temp);
			close(downloadfd);

		}
}
void share(int sockfd)
{
	char buff[256]="2";
	int n;
	n = write(sockfd, buff, strlen(buff));
	if(n<0){
		cout<<"Write Error\n";
	}
	cout<<"Enter Filename : \n>> ";
	bzero(buff,256);
	cin>>buff;
	n = write(sockfd, buff, strlen(buff));
	if(n<0){
		cout<<"Write Error\n";
	}
	bzero(buff,256);
	n = read(sockfd,buff,255);
	cout<<buff;
	return;

}
void closeme(int sockfd)
{
	char buff[2]="3";
	int n;
	n = write(sockfd, buff, strlen(buff));
	if(n<0){
		cout<<"Write Error\n";
	}
	return;
}
/*void sigproc(int sig)
{ 		 signal(SIGINT, sigproc);   
		 
 
		 printf("Server has resumed me\n");
}*/
int main(int argc,char *argv[])
{
	if(argc<3)
	{
		cout<<"Format : "<<argv[0]<<" RepoServerIP PortNum\n";
		return 0;
	}
	int sockfd, portno, n;
	int t=1;
	char buff[256];

	int x=fork();
	/*Download Server*/
	if(x==0)
	{

		sockfd=socketCreateBind(PORT_NUM);
		/*if(kill(getppid(), SIGINT)==0)
		{
			cout<<"Client signalled to start";
		}*/
		if(sockfd>0){
			server1(sockfd);
			close(sockfd);
		}
		
	}
	//Client-Repo Communication
	if(x!=0){
		
		/*signal(SIGINT, sigproc);
		sigset_t mask;

 
		sigemptyset (&mask);
		sigaddset (&mask, SIGINT);
		sigsuspend(&mask);*/
		sleep(2);
		/*Socket Creation*/
		sockfd=socket(AF_INET,SOCK_STREAM,0);
		if(sockfd<0)
			cout<<"Socket cannot be created Error\n";
		
		portno=atoi(argv[2]);
		string serverIP;
		if(connectToServer(sockfd,argv[1],portno)<0)
		{
			return 0;
		}
		
		//Repo
		char option;
		int flag=0;
		while(1)
		{
			cout<<"\nSelect :\n";
			cout<<"1. Search\n2. Share\n3. Exit\n\n>>";
			cin>>option;
			switch(option)
			{
				case '1':	search(sockfd);
						
						break;
				case '2':	share(sockfd);
						break;
				case '3':	closeme(sockfd);
						close(sockfd);
						flag=1;
						break;
				default:cout<<"Please choose a vaild option.\n";

			}
			if(flag==1)
				break;

		}
		

		/*cout<<"\n\nDownload Server is still on!\nChoose:\nPress 1 continue host server\nPress any other key Terminate host server as well\n";
		cin>>option;
		if(option=='1')
		{
			//waitpid(-1,x,0);
		}
		else
		{*/
			close(sockfd);

		//}
		//	close(sockfd);
	}
	return 0;
}