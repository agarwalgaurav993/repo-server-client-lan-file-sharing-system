/*Gaurav Agarwal - 20162088*/
#include <iostream>
#include <algorithm>
#include <map>
#include <vector>
#include <unistd.h>
#include <string.h>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <time.h>
/*System calls Network*/
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <fcntl.h>
#include <sys/uio.h>
#include <sys/stat.h>

using namespace std;

vector<pair<string,string> > files;

/*Populate Repo.txt into memory*/
void split(string &s, char delim, pair<string, string> &elems) {
    istringstream ss;
    //stringstream ss;
    ss.str(s);
    string item;
    getline(ss, item, delim) ;
        elems.first=item;
    getline(ss, item, delim) ;
        elems.second=item;
}
void populate()
{
	files.clear();
	int fd;
	ifstream t;
	t.open("repo.txt");
	string line;
	//vector <string> ar;
	pair <string,string> elems;
	while(!t.eof())
	{
		getline(t, line,'\n');
		split(line,'@',elems);
		files.push_back(elems);


	}
	t.close();

}

/*Search Logic*/
void search(string name, vector<pair<string,string> > &searchRes)
{
	for(int i=0;i<files.size();i++)
	{
		if(files[i].first.find(name)+1)
			searchRes.push_back(files[i]);
	}
	cout<<"\nSearching result for name : "<<name<<endl;
	
}

/*Log Logic*/
void log(string IP,string operation)
{
	
	time_t rawtime;
	time (&rawtime);
	string time(ctime(&rawtime));
	time[time.length()-1]=' ';
	IP = time + ":" + operation +  IP + "\n";
	fstream fs;
  	fs.open ("repo.log",fstream::out|fstream::app);
  	fs << IP;

}


/*Add Logic*/
void add(string IP,string filepath)
{
	log(IP,filepath + "Shared by ");
	pair <string,string> p1;
	p1=make_pair(filepath,IP);
	files.push_back(p1);
	filepath = filepath+"@"+IP+"\n";
	fstream fs;
  	fs.open ("repo.txt",fstream::out|fstream::app);

  	fs << filepath;

  	fs.close();

}


/*Search Handle with Client*/
void searchHandler(int clientfd,string clientIP)
{

	char buff[256];
	
	bzero(buff,256);
	vector<pair<string,string> > searchRes;
	int n;
	bzero(buff,256);
	string name;
	if(n=read(clientfd,buff,255))
		name=string(buff);
	
	/*Log Call*/
	log(clientIP,"Search of "+name+" requested by :");
	cout<<"Search result for name : "<<name<<endl;
	search(name,searchRes);
	cout<<"Found "<<searchRes.size()<<" macthing entries in repo.txt\n";
	
	bzero(buff,256);
	//itoa(,buff,10);
	sprintf(buff,"%ld", searchRes.size());
	write(clientfd, buff, strlen(buff));
	//cout<<"Written No:"<<buff<<endl;
	read(clientfd,buff,255);
	for(int i=0;i<searchRes.size();i++)
	{
		
		searchRes[i].first=searchRes[i].first+"\t"+searchRes[i].second;
		bzero(buff,256);
		strcpy(buff, searchRes[i].first.c_str());
		write(clientfd,buff,strlen(buff));
		sleep(1);
	}

	return;
}

/*Request Identifier*/
void requestIdentify(int clientfd,string clientIP)
{
	char buff[256];
	int n;
	bzero(buff,256);
	while(1)
	while(n=read(clientfd,buff,255))//Request
	{
		if(n<0)
		{
			cout<<"Read Error\n";
			return;
		}
		switch(buff[0])
		{
			case '1':	//SearchHandler+AppropiateLog
						cout<<"Search Requested \n";
						/*Populating Vector */
						populate();
						searchHandler(clientfd,clientIP);
						cout<<"Search served \n";
						break;
			case '2':	//Add+AppropiateLog
						cout<<"Share Requested \n";
						bzero(buff,256);
						n=read(clientfd,buff,255);
						add(clientIP,string(buff));
						write(clientfd,"Successfully shared the file",28);
						cout<<"Shared file \n";
						break;

			case '3':	cout<<"Exit Requested \n";
						return;//Return and serve next clent in queue
			
			default : cout<<"Improper Request by Client\n";
						break;

		}
		bzero(buff,256);

	}
	
}

/*Server Code*/
int main(int argc,char* argv[])
{
	if(argc<2)
	{
		cout<<"Format : "<<argv[0]<<" PortNum";
		return 0;
	}
	int sockfd, clientfd, portno;
	socklen_t clilen;
	char buff[256];
	int t=1;
	struct sockaddr_in serv_addr,cli_addr;

	

	/*Socket Creation*/
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0){
		cout<<"Socket Creation Error";
		exit(1);
	}
	
	/*Server Address Initialization*/
	bzero((char*) &serv_addr,sizeof(serv_addr));
	portno=atoi(argv[1]);
	serv_addr.sin_family=AF_INET;
	serv_addr.sin_addr.s_addr=INADDR_ANY;
	serv_addr.sin_port=htons(portno);
	/*Socket Binds to address*/
	if(bind(sockfd,(struct sockaddr*) &serv_addr,sizeof(serv_addr))<0){
		cout<<"Port already in use : Bind error\n";
		return 0;
	}
	else
	{
		cout<< "Bind Completed\n";
	}

	int pid;
	/*Server Waiting*/
	while(1)
	{

		pid=-1;
		cout<<"Waiting for a new client\n";
		listen(sockfd,5);
		clilen = sizeof(cli_addr);
		
		clientfd = accept(sockfd,(struct sockaddr*) &cli_addr, &clilen);
		if(clientfd<0)
			cout<<"Accept Error";
		else{
			cout<<"Client Accepted : ";
			pid=fork();
		}
		if(pid<0)
			cout<<"Request cannot be served at the moment\n";
		if(pid==0)
		{
			
			string clientIP(inet_ntoa(cli_addr.sin_addr));
			cout<<clientIP<<"\n";
			/*Search Logic*/
			requestIdentify(clientfd,clientIP);
			close(clientfd);
			cout<<"Client connection terminated\n";
			_exit(0);
		}
	
		
    }
	close(sockfd);
	
	return 0;
}
